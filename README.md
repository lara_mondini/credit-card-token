# API Criptografia em Dados Sensíveis

Essa API foi criada com o intuito armazenar no banco de dados dados sensíveis criptografados. Ela está publicada no servidor Heroku.

```sh
https://credit-card-token.herokuapp.com/
```

### Detalhes API

A tabela do banco de dados tem os seguintes campos.

| Campo             |  Tipo     | Dado Sensível |
| ----------------  | -------   |-------------- |
|  id               |  Long     | não           |
| userDocument      |  String   | sim           |
|creditCardToken    | String    | sim           |
|value              | Long      | não           |

O projeto foi desenvolvido utilizando o framework spring boot e o banco de dados postgres.

Para rodar o projeto, primeiro é necessário alterar a configuração do banco de dados Postgres no arquivo de propriedades (src/main/resources/application.properties).

```sh
spring.datasource.url=jdbc:postgresql://{ip conexao banco}:{porta}/{nome do banco}
spring.datasource.username={usuario}
spring.datasource.password={senha}
```

Feito isso, é necessário executar o seguinte comando para que o maven instale as dependências necessárias.

```sh
$ mvn clean install
```

Para iniciar a aplicação é necessário executar o seguinte comando:

```sh
$  mvn spring-boot:run
```

O projeto está rodando na porta 8081.

OBS: Para executar o código na IDE é necessário instalar o [lombok](https://projectlombok.org/).

Contratos API:
```sh
Inserir Dados
[PUT] /creditcardtoken

Content-type: application/json

{
    "userDocument": "123459",
    "token": "4577899999",
    "value": 10000
}
```

```sh
Atualizar Dados
[POST] /creditcardtoken

Content-type: application/json

{
    "id": 29,
    "userDocument": "123459",
    "token": "4577899999",
    "value": 10000
}
```

```sh
Listar/Buscar Dados
[GET] /creditcardtoken
[GET] /creditcardtoken/<id>

Content-type: application/json

```

```sh
Deletar Dados
[DELETE] /creditcardtoken/<id>

Content-type: application/json

```