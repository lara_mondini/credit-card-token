package br.com.project.creditcard.token.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.project.creditcard.token.dto.CreditCardTokenRequest;
import br.com.project.creditcard.token.dto.CreditCardTokenResponse;
import br.com.project.creditcard.token.exception.CreditCardTokenException;
import br.com.project.creditcard.token.model.CreditCardToken;
import br.com.project.creditcard.token.service.CreditCardTokenService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

@Slf4j
@RestController
@RequestMapping("/creditcardtoken")
public class CreditCardTokenController {

	private CreditCardTokenService service;

	@Autowired
	public CreditCardTokenController(CreditCardTokenService service) {
		this.service = service;
	}

	@PutMapping
	public ResponseEntity<CreditCardTokenResponse> addCreditCardToken(
			@Valid @RequestBody CreditCardTokenRequest cardToken) throws CreditCardTokenException {
		log.info("Adding credit card token ...");

		return ResponseEntity.ok().body(service.addCreditCardToken(cardToken));
	}

	@PostMapping
	public ResponseEntity<CreditCardTokenResponse> updateCreditCardToken(
			@Valid @RequestBody CreditCardTokenRequest cardToken) throws CreditCardTokenException {
		log.info("Updating credit card token ID: {}", cardToken.getId());

		return ResponseEntity.ok().body(service.updateCreditCardToken(cardToken));
	}

	@GetMapping(value = { "", "/{id}" })
	public ResponseEntity<List<CreditCardToken>> getCreditCardsToken(
			@PathVariable(name = "id", required = false) Long id) throws CreditCardTokenException {
		log.info("Get all credit cards token or filter by ID {}", id);

		return ResponseEntity.ok().body(service.getCreditCardsToken(id));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCreditCardToken(@PathVariable(name = "id", required = true) Long id)
			throws CreditCardTokenException {
		log.info("Deleting credit card token ID: {}", id);

		service.deleteCreditCardToken(id);

		return ResponseEntity.ok().build();
	}

}
