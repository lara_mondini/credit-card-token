package br.com.project.creditcard.token.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditCardTokenRequest {

	private Long id;

	@NotBlank
	private String userDocument;

	@NotBlank
	private String token;

	@NotNull
	private Long value;

}
