package br.com.project.creditcard.token.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public class CryptographySha512 {

	private CryptographySha512() {
	}

	public static String encrypt(String value) throws NoSuchAlgorithmException {
		if (value == null) {
			throw new IllegalArgumentException("O valor está nulo.");
		}

		MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
		byte[] hash = messageDigest.digest(value.getBytes());

		return Base64.getEncoder().encodeToString(hash);
	}
}
