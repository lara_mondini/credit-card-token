package br.com.project.creditcard.token.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* @author mondini.lara@gmail.com
* @version 1.0
* @since 1.0
*/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditCardTokenResponse {
	
	private Long id;
	
}
