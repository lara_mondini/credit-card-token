package br.com.project.creditcard.token.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class CreditCardTokenException extends Exception {

	private static final long serialVersionUID = 6155255423587781092L;

	public CreditCardTokenException(String message) {
		super(message);
	}
}
