package br.com.project.creditcard.token.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.project.creditcard.token.model.CreditCardToken;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

@Repository
public interface CreditCardTokenRepository extends JpaRepository<CreditCardToken, Long> {

}
