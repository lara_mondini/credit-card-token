package br.com.project.creditcard.token.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.project.creditcard.token.converter.SensitiveFieldsConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "creditCardToken")
public class CreditCardToken {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "userDocument")
	@Convert(converter = SensitiveFieldsConverter.class)
	private String userDocument;

	@NotNull
	@Column(name = "creditCardToken")
	@Convert(converter = SensitiveFieldsConverter.class)
	private String token;

	@NotNull
	@Column(name = "value")
	private Long value;

}
