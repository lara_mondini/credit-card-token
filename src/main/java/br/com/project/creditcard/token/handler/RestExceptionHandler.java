package br.com.project.creditcard.token.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.project.creditcard.token.exception.CreditCardTokenException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(CreditCardTokenException.class)
	public ResponseEntity<ErrorDetail> handleResourceNotFoundException(CreditCardTokenException exception,
			HttpServletRequest request) {

		ErrorDetail errorDetail = new ErrorDetail(System.currentTimeMillis(), HttpStatus.UNPROCESSABLE_ENTITY.value(),
				HttpStatus.UNPROCESSABLE_ENTITY.name(), exception.getMessage(), request.getRequestURI());

		log.error("Erro: [{}] no path: [{}]", errorDetail.getMessage(), errorDetail.getPath());

		return new ResponseEntity<>(errorDetail, null, HttpStatus.UNPROCESSABLE_ENTITY);
	}
}

@AllArgsConstructor
@Data
class ErrorDetail {

	private Long timestamp;

	private int status;

	private String error;

	private String message;

	private String path;

}