package br.com.project.creditcard.token;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectCreditCardTokenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCreditCardTokenApplication.class, args);
	}

}
