package br.com.project.creditcard.token.converter;

import javax.persistence.AttributeConverter;

import br.com.project.creditcard.token.security.CryptographySha512;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public class SensitiveFieldsConverter implements AttributeConverter<String, String> {

	@Override
	public String convertToDatabaseColumn(String attribute) {
		try {
			return CryptographySha512.encrypt(attribute);
		} catch (Exception e) {
			return "";
		}
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		return dbData;
	}

}
