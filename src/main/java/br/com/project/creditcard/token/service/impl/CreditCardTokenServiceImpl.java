package br.com.project.creditcard.token.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.project.creditcard.token.converter.CreditCardTokenConverter;
import br.com.project.creditcard.token.dto.CreditCardTokenRequest;
import br.com.project.creditcard.token.dto.CreditCardTokenResponse;
import br.com.project.creditcard.token.exception.CreditCardTokenException;
import br.com.project.creditcard.token.model.CreditCardToken;
import br.com.project.creditcard.token.repository.CreditCardTokenRepository;
import br.com.project.creditcard.token.service.CreditCardTokenService;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

@Service
public class CreditCardTokenServiceImpl implements CreditCardTokenService {

	private CreditCardTokenRepository repository;

	private static final String EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND = "ID não encontrado na base de dados: ";
	private static final String EXPECTED_MESSAGE_EXCEPTION_ID_NULL = "ID não pode ser nulo";

	@Autowired
	public CreditCardTokenServiceImpl(CreditCardTokenRepository repository) {
		this.repository = repository;
	}

	@Override
	public CreditCardTokenResponse addCreditCardToken(CreditCardTokenRequest card) throws CreditCardTokenException {
		CreditCardToken saved = repository.saveAndFlush(CreditCardTokenConverter.requestToModel(card));

		return CreditCardTokenResponse.builder().id(saved.getId()).build();
	}

	@Override
	public CreditCardTokenResponse updateCreditCardToken(CreditCardTokenRequest card) throws CreditCardTokenException {
		if (card.getId() == null) {
			throw new CreditCardTokenException(EXPECTED_MESSAGE_EXCEPTION_ID_NULL);
		}

		Optional<CreditCardToken> tokenCardOpt = repository.findById(card.getId());

		if (tokenCardOpt.isPresent()) {
			tokenCardOpt.get().setToken(card.getToken());
			tokenCardOpt.get().setUserDocument(card.getUserDocument());
			tokenCardOpt.get().setValue(card.getValue());

			CreditCardToken saved = repository.saveAndFlush(tokenCardOpt.get());

			return CreditCardTokenResponse.builder().id(saved.getId()).build();
		}

		throw new CreditCardTokenException(EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND + card.getId());
	}

	@Override
	public List<CreditCardToken> getCreditCardsToken(Long id) throws CreditCardTokenException {
		if (id == null) {
			return repository.findAll();
		}

		Optional<CreditCardToken> tokenCardOpt = repository.findById(id);

		if (tokenCardOpt.isPresent()) {
			return Arrays.asList(tokenCardOpt.get());
		}

		throw new CreditCardTokenException(EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND + id);
	}

	@Override
	public void deleteCreditCardToken(Long id) throws CreditCardTokenException {
		if (id != null && repository.findById(id).isPresent()) {
			repository.deleteById(id);
		} else {
			throw new CreditCardTokenException(EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND + id);
		}
	}
}
