package br.com.project.creditcard.token.converter;

import br.com.project.creditcard.token.dto.CreditCardTokenRequest;
import br.com.project.creditcard.token.model.CreditCardToken;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public class CreditCardTokenConverter {

	private CreditCardTokenConverter() {
	}

	public static CreditCardToken requestToModel(CreditCardTokenRequest request) {
		// @formatter:off
		return CreditCardToken.builder()
				.id(request.getId())
				.token(request.getToken())
				.userDocument(request.getUserDocument())
				.value(request.getValue())
				.build();
		// @formatter:on
	}

}
