package br.com.project.creditcard.token.service;

import java.util.List;

import br.com.project.creditcard.token.dto.CreditCardTokenRequest;
import br.com.project.creditcard.token.dto.CreditCardTokenResponse;
import br.com.project.creditcard.token.exception.CreditCardTokenException;
import br.com.project.creditcard.token.model.CreditCardToken;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public interface CreditCardTokenService {

	CreditCardTokenResponse updateCreditCardToken(CreditCardTokenRequest card) throws CreditCardTokenException;

	CreditCardTokenResponse addCreditCardToken(CreditCardTokenRequest card) throws CreditCardTokenException;

	List<CreditCardToken> getCreditCardsToken(Long id) throws CreditCardTokenException;

	void deleteCreditCardToken(Long id) throws CreditCardTokenException;

}
