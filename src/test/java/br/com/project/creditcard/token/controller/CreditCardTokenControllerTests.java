package br.com.project.creditcard.token.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.project.creditcard.token.mock.MockObjectsCreditCardToken;
import br.com.project.creditcard.token.service.CreditCardTokenService;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */
@TestInstance(Lifecycle.PER_CLASS)
public class CreditCardTokenControllerTests {

	private CreditCardTokenController controller;

	private MockMvc mockMvc;

	@Mock
	private CreditCardTokenService service;

	@BeforeAll
	public void init() {
		MockitoAnnotations.openMocks(this);
		controller = new CreditCardTokenController(service);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void testPutCreditCardToken() throws Exception {
		when(service.addCreditCardToken(Mockito.any()))
				.thenReturn(MockObjectsCreditCardToken.creditCardTokenResponse());

		String jsonRequest = new ObjectMapper().writeValueAsString(MockObjectsCreditCardToken.creditCardTokenRequest());

		String jsonRespose = new ObjectMapper()
				.writeValueAsString(MockObjectsCreditCardToken.creditCardTokenResponse());

		// @formatter:off
		this.mockMvc.perform(put("/creditcardtoken")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest))
					.andExpect(status().isOk())
					.andExpect(content().json(jsonRespose));
		// @formatter:on
	}

	@Test
	public void testPostCreditCardToken() throws Exception {
		when(service.updateCreditCardToken(Mockito.any()))
				.thenReturn(MockObjectsCreditCardToken.creditCardTokenResponse());

		String jsonRequest = new ObjectMapper().writeValueAsString(MockObjectsCreditCardToken.creditCardTokenRequest());

		String jsonResponse = new ObjectMapper()
				.writeValueAsString(MockObjectsCreditCardToken.creditCardTokenResponse());

		// @formatter:off
		this.mockMvc.perform(post("/creditcardtoken")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest))
					.andExpect(status().isOk())
					.andExpect(content().json(jsonResponse));
		// @formatter:on
	}

	@Test
	public void testGetCreditCardToken() throws Exception {
		// @formatter:off
		this.mockMvc.perform(get("/creditcardtoken")
				.contentType(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk())
					.andExpect(content().json("[]"));
		// @formatter:on
	}

	@Test
	public void testDeleteCreditCardToken() throws Exception {
		doNothing().when(service).deleteCreditCardToken(Mockito.anyLong());

		// @formatter:off
		this.mockMvc.perform(delete("/creditcardtoken/1")
				.contentType(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk());
		// @formatter:on
	}

}
