package br.com.project.creditcard.token.security;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.NoSuchAlgorithmException;

import org.junit.jupiter.api.Test;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public class CryptographySha512Tests {

	@Test
	public void testExceptionCryptographySha512() throws NoSuchAlgorithmException {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			CryptographySha512.encrypt(null);
		});

		String expectedMessage = "O valor está nulo.";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void testCryptographySha512() throws NoSuchAlgorithmException {
		assertNotNull(CryptographySha512.encrypt(""));
	}

}
