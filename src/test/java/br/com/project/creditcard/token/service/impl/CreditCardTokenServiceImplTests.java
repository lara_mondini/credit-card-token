package br.com.project.creditcard.token.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.project.creditcard.token.dto.CreditCardTokenRequest;
import br.com.project.creditcard.token.dto.CreditCardTokenResponse;
import br.com.project.creditcard.token.exception.CreditCardTokenException;
import br.com.project.creditcard.token.mock.MockObjectsCreditCardToken;
import br.com.project.creditcard.token.model.CreditCardToken;
import br.com.project.creditcard.token.repository.CreditCardTokenRepository;
import br.com.project.creditcard.token.service.CreditCardTokenService;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

@TestInstance(Lifecycle.PER_CLASS)
public class CreditCardTokenServiceImplTests {

	@Mock
	private CreditCardTokenRepository repository;

	private CreditCardTokenService service;

	private static final String EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND = "ID não encontrado na base de dados:";
	private static final String EXPECTED_MESSAGE_EXCEPTION_ID_NULL = "ID não pode ser nulo";

	@BeforeAll
	public void init() {
		MockitoAnnotations.openMocks(this);
		service = new CreditCardTokenServiceImpl(repository);
	}

	@Test
	public void testAddCreditCardToken() throws CreditCardTokenException {
		CreditCardToken model = MockObjectsCreditCardToken.creditCardToken();

		when(repository.saveAndFlush(Mockito.any(CreditCardToken.class))).thenReturn(model);

		CreditCardTokenResponse response = service.addCreditCardToken(mock(CreditCardTokenRequest.class));

		assertNotNull(response);
		assertEquals(model.getId(), response.getId());
	}

	@Test
	public void testUpdateCreditCardToken() throws CreditCardTokenException {
		CreditCardToken model = MockObjectsCreditCardToken.creditCardToken();

		when(repository.findById(Mockito.any())).thenReturn(MockObjectsCreditCardToken.optionalCreditCardToken());
		when(repository.saveAndFlush(Mockito.any(CreditCardToken.class))).thenReturn(model);

		CreditCardTokenResponse response = service.updateCreditCardToken(mock(CreditCardTokenRequest.class));

		assertNotNull(response);
		assertEquals(model.getId(), response.getId());
	}

	@Test
	public void testUpdateCreditCardTokenExceptionIdNull() throws CreditCardTokenException {
		CreditCardTokenException exception = assertThrows(CreditCardTokenException.class, () -> {
			service.updateCreditCardToken(MockObjectsCreditCardToken.creditCardTokenRequestIdNull());
		});

		assertTrue(exception.getMessage().contains(EXPECTED_MESSAGE_EXCEPTION_ID_NULL));
	}

	@Test
	public void testUpdateCreditCardTokenException() throws CreditCardTokenException {
		when(repository.findById(Mockito.any())).thenReturn(Optional.empty());

		CreditCardTokenException exception = assertThrows(CreditCardTokenException.class, () -> {
			service.updateCreditCardToken(MockObjectsCreditCardToken.creditCardTokenRequest());
		});

		assertTrue(exception.getMessage().contains(EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND));
	}

	@Test
	public void testGetAllCreditCardsToken() throws CreditCardTokenException {
		List<CreditCardToken> listMock = MockObjectsCreditCardToken.listThreeCreditCardToken();

		when(repository.findAll()).thenReturn(listMock);

		List<CreditCardToken> listResponse = service.getCreditCardsToken(null);

		assertNotNull(listResponse);
		assertEquals(3, listResponse.size());
	}

	@Test
	public void testGetCreditCardsTokenById() throws CreditCardTokenException {
		when(repository.findById(Mockito.any())).thenReturn(MockObjectsCreditCardToken.optionalCreditCardToken());

		List<CreditCardToken> listResponse = service.getCreditCardsToken(1L);

		assertNotNull(listResponse);
		assertEquals(1, listResponse.size());
	}

	@Test
	public void testGetCreditCardsTokenWithNoData() throws CreditCardTokenException {
		when(repository.findById(Mockito.any())).thenReturn(Optional.empty());

		CreditCardTokenException exception = assertThrows(CreditCardTokenException.class, () -> {
			service.getCreditCardsToken(10L);
		});

		assertTrue(exception.getMessage().contains(EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND));
	}

	@Test
	public void testDeleteCreditCardToken() throws CreditCardTokenException {
		when(repository.findById(Mockito.any())).thenReturn(MockObjectsCreditCardToken.optionalCreditCardToken());

		service.deleteCreditCardToken(Long.MIN_VALUE);

		assertTrue(true);
	}

	@Test
	public void testDeleteCreditCardTokenWithIdNull() throws CreditCardTokenException {
		CreditCardTokenException exception = assertThrows(CreditCardTokenException.class, () -> {
			service.deleteCreditCardToken(null);
		});

		assertTrue(exception.getMessage().contains(EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND));
	}

	@Test
	public void testDeleteCreditCardTokenWithoutItem() throws CreditCardTokenException {
		when(repository.findById(Mockito.any())).thenReturn(Optional.empty());

		CreditCardTokenException exception = assertThrows(CreditCardTokenException.class, () -> {
			service.deleteCreditCardToken(10L);
		});

		assertTrue(exception.getMessage().contains(EXPECTED_MESSAGE_EXCEPTION_ID_NOT_FOUND));
	}
}
