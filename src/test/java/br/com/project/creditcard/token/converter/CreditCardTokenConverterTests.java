package br.com.project.creditcard.token.converter;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import br.com.project.creditcard.token.dto.CreditCardTokenRequest;
import br.com.project.creditcard.token.mock.MockObjectsCreditCardToken;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public class CreditCardTokenConverterTests {

	@Test
	public void testConverterRequestToModel() {
		CreditCardTokenRequest request = MockObjectsCreditCardToken.creditCardTokenRequest();

		assertNotNull(CreditCardTokenConverter.requestToModel(request));
	}

}
