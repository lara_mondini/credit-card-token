package br.com.project.creditcard.token.converter;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public class SensitiveFieldsConverterTests {

	private SensitiveFieldsConverter converter = new SensitiveFieldsConverter();

	@Test
	public void testConvertToEntityAttribute() {
		String converted = converter.convertToEntityAttribute("");
		assertNotNull(converted);
		assertEquals("", converted);
	}

	@Test
	public void testConvertToDatabaseColumnInvalid() {
		String converted = converter.convertToDatabaseColumn(null);
		assertNotNull(converted);
		assertEquals("", converted);
	}

	@Test
	public void testConvertToDatabaseColumn() {
		String converted = converter.convertToDatabaseColumn("");
		assertNotNull(converted);
		assertNotEquals("", converted);
	}
}
