package br.com.project.creditcard.token.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.project.creditcard.token.dto.CreditCardTokenRequest;
import br.com.project.creditcard.token.dto.CreditCardTokenResponse;
import br.com.project.creditcard.token.model.CreditCardToken;

/**
 * @author mondini.lara@gmail.com
 * @version 1.0
 * @since 1.0
 */

public class MockObjectsCreditCardToken {

	public static CreditCardToken creditCardToken() {
		return CreditCardToken.builder().id(1L).userDocument("").token("").value(100L).build();
	}

	public static CreditCardTokenRequest creditCardTokenRequest() {
		return CreditCardTokenRequest.builder().id(1L).token("token").value(10L).userDocument("userDocument").build();
	}

	public static List<CreditCardToken> listThreeCreditCardToken() {
		List<CreditCardToken> list = new ArrayList<>();
		list.add(creditCardToken());
		list.add(creditCardToken());
		list.add(creditCardToken());

		return list;
	}

	public static Optional<CreditCardToken> optionalCreditCardToken() {
		return Optional.of(creditCardToken());
	}

	public static CreditCardTokenResponse creditCardTokenResponse() {
		return CreditCardTokenResponse.builder().id(1L).build();
	}

	public static CreditCardTokenRequest creditCardTokenRequestIdNull() {
		return CreditCardTokenRequest.builder().id(null).userDocument("").token("").value(100L).build();
	}

}
